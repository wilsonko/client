﻿namespace ClientLicenseTool
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    public class RestEndpoint
    {
        HttpClient          m_Client;
        HttpResponseMessage m_Response;
        bool                m_IsSuccessful;
        bool                m_LoggingEnabled;

        public string   urlAddress      { get; set; }
        public string   urlParameters   { get; set; }
        public string   LicenseFile     { get; set; }

        public RestEndpoint(bool loggingEnabled)
        {
            m_LoggingEnabled = loggingEnabled;
        }
 
        public async Task Query(GuiForm form)
        {
            m_Client                    = new HttpClient();
            m_Client.BaseAddress        = new Uri(urlAddress);
            m_Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            m_Response                  = null;

            HttpRequestMessage request  = new HttpRequestMessage();
            request.Method              = HttpMethod.Post;
            request.Content             = new StringContent(urlParameters, Encoding.UTF8);

            form.ChangeStatus("Querying");

            try
            {
                m_Response      = await m_Client.SendAsync(request);
                m_IsSuccessful  = m_Response.IsSuccessStatusCode;
            }
            catch (HttpRequestException e)
            {
                if (m_Response == null)
                {
                    form.ChangeStatus("Connection Failure");

                    Logging.Write("Exception: [" + e.ToString() + "]", true);
                    return;
                }

                Logging.Write("Exception: [" + e.ToString() + "]", true);
            }
             
            if (m_IsSuccessful)
            {
                form.ChangeStatus("Downloading");

                byte[] byteResponse = await m_Response.Content.ReadAsByteArrayAsync();
                string urlResponse  = System.Text.Encoding.Default.GetString(byteResponse);

                string licensePath  = Path.GetDirectoryName(LicenseFile);
                if (!Directory.Exists(licensePath))
                {
                    Directory.CreateDirectory(licensePath);
                }

                using (StreamWriter writer = new StreamWriter(LicenseFile, false))
                {
                    try
                    {
                        writer.WriteLine(urlResponse);

                        form.ChangeStatus("Completed. License Ready.");

                        Logging.Write("Generated License File [" + LicenseFile + "]", m_LoggingEnabled);
                    }
                    catch (Exception ex)
                    {
                        form.ChangeStatus("Failed [" + m_Response.StatusCode + "]");

                        Logging.Write("Failed to generate license file: [" + ex.ToString() + "]", true);
                    }
                }
            }
            else
            {
                form.ChangeStatus("Failed [" + m_Response.StatusCode + "]");

                Logging.Write("Response from server [" + m_Response.StatusCode + "] + [" + m_Response.ReasonPhrase + "]", true);
            }
        }
    }
}
