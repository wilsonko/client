﻿namespace ClientLicenseTool
{
    using System;
    using System.IO;

    public class Logging
    {
        private static string BuildLogName(string fileName = null)
        {
            if (!string.IsNullOrEmpty(fileName) && Directory.Exists(Path.GetFullPath(fileName)))
            {
                return fileName;
            }
            else
            {
                return Path.Combine(Directory.GetCurrentDirectory(), "Log.txt");
            }
        }

        public static void Reset(string fileName = null)
        {
            string logFile = BuildLogName(fileName);

            if (File.Exists(logFile))
            {
                try
                {
                    File.Delete(logFile);
                }
                catch
                {
                    // Do nothing
                }
            }
        }

        public static void Write(string message, bool enabled, string fileName = null)
        {
            string logFile = BuildLogName(fileName);

            if (!enabled)
            {
                return;
            }

            for (int retry = 0; retry < 10; retry++)
            {
                try
                {
                    using (StreamWriter writer = new StreamWriter(logFile, true))
                    {
                        writer.WriteLine(DateTime.Now.ToString() + "  " + message);
                    }

                    return;
                }
                catch
                {
                    System.Threading.Thread.Sleep(1000);
                    // Do nothing
                }
            }
        }
    }
}
