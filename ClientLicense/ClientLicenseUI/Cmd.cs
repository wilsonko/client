﻿namespace ClientLicenseTool
{
    using System.Diagnostics;
    using System.IO;

    public class CMD
    {
        Process m_Process;
        bool    m_IsValid;

        public CMD()
        {
            m_Process = new Process();
            m_IsValid = false;
        }

        public void Dispose()
        {
            if (!m_IsValid)
            {
                return;
            }

            if (!m_Process.HasExited)
            {
                m_Process.Kill();

                m_Process.WaitForExit();
            }
        }

        public string StandardOutput { private set; get; }

        public bool Run(string exe, string args, bool redirect = false, bool verbose = false)
        {
            m_IsValid = true;

            try
            {
                m_Process.StartInfo.FileName               = exe;
                m_Process.StartInfo.Arguments              = args;
                m_Process.StartInfo.CreateNoWindow         = true;
                m_Process.StartInfo.UseShellExecute        = false;
                m_Process.StartInfo.RedirectStandardOutput = redirect;
                m_Process.Start();

                if (redirect)
                {
                    StreamReader output = m_Process.StandardOutput;
                    this.StandardOutput = output.ReadToEnd();
                }

                m_Process.WaitForExit();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
