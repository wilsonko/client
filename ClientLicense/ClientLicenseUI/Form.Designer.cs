﻿namespace ClientLicenseTool
{
    partial class GuiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.text_SerialNumber = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_Request = new System.Windows.Forms.Button();
            this.group_IdentifierCode = new System.Windows.Forms.GroupBox();
            this.text_IdentifierCode = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.text_Status = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.group_IdentifierCode.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.text_SerialNumber);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(408, 56);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Number";
            // 
            // text_SerialNumber
            // 
            this.text_SerialNumber.Location = new System.Drawing.Point(6, 19);
            this.text_SerialNumber.Name = "text_SerialNumber";
            this.text_SerialNumber.Size = new System.Drawing.Size(396, 20);
            this.text_SerialNumber.TabIndex = 1;
            this.text_SerialNumber.TextChanged += new System.EventHandler(this.text_SerialNumber_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_Request);
            this.groupBox2.Location = new System.Drawing.Point(493, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(89, 56);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "License";
            // 
            // button_Request
            // 
            this.button_Request.Location = new System.Drawing.Point(6, 17);
            this.button_Request.Name = "button_Request";
            this.button_Request.Size = new System.Drawing.Size(75, 23);
            this.button_Request.TabIndex = 2;
            this.button_Request.Text = "Request";
            this.button_Request.UseVisualStyleBackColor = true;
            this.button_Request.Click += new System.EventHandler(this.button_Request_Click);
            // 
            // group_IdentifierCode
            // 
            this.group_IdentifierCode.Controls.Add(this.text_IdentifierCode);
            this.group_IdentifierCode.Location = new System.Drawing.Point(12, 74);
            this.group_IdentifierCode.Name = "group_IdentifierCode";
            this.group_IdentifierCode.Size = new System.Drawing.Size(475, 56);
            this.group_IdentifierCode.TabIndex = 4;
            this.group_IdentifierCode.TabStop = false;
            this.group_IdentifierCode.Text = "Identifier Code";
            // 
            // text_IdentifierCode
            // 
            this.text_IdentifierCode.Location = new System.Drawing.Point(6, 19);
            this.text_IdentifierCode.Name = "text_IdentifierCode";
            this.text_IdentifierCode.ReadOnly = true;
            this.text_IdentifierCode.Size = new System.Drawing.Size(462, 20);
            this.text_IdentifierCode.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.text_Status);
            this.groupBox3.Location = new System.Drawing.Point(426, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(156, 56);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Status";
            // 
            // text_Status
            // 
            this.text_Status.Location = new System.Drawing.Point(6, 19);
            this.text_Status.Name = "text_Status";
            this.text_Status.Size = new System.Drawing.Size(143, 20);
            this.text_Status.TabIndex = 0;
            // 
            // GuiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 137);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.group_IdentifierCode);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "GuiForm";
            this.Text = "Client License Tool";
            this.Load += new System.EventHandler(this.Form_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.group_IdentifierCode.ResumeLayout(false);
            this.group_IdentifierCode.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox text_SerialNumber;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_Request;
        private System.Windows.Forms.GroupBox group_IdentifierCode;
        private System.Windows.Forms.TextBox text_IdentifierCode;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox text_Status;
    }
}

