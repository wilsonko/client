﻿// Minimum .NET framework required is 4.5
namespace ClientLicenseTool
{
    using System;
    using System.Configuration;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    public partial class GuiForm : System.Windows.Forms.Form
    {
        RestEndpoint    m_RestEndpoint;
        string          m_UrlAddress;
        bool            m_LoggingEnabled;
        string          m_LicenseFile;

        public GuiForm()
        {
            InitializeComponent();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            button_Request.Enabled  = false;
            text_Status.Text        = string.Empty;
            text_Status.ReadOnly    = true;

            // https://msdn.microsoft.com/en-us/library/windows/desktop/ms724832(v=vs.85).aspx
            if ((Environment.OSVersion.Version.Major == 6 && Environment.OSVersion.Version.Minor == 0) &&
                 Environment.OSVersion.Version.Major <  6)
            {
                Logging.Reset();
                Logging.Write("Running on Unsupported OS: [" + Environment.OSVersion.Version.Major + "] [" + Environment.OSVersion.Version.Minor + "]", true);

                this.Close();
            }

            string urlDefault   = "http://ec2-54-166-245-70.compute-1.amazonaws.com/pomelo/register_signature";
            m_UrlAddress        = ParseStringConfig("UrlAddress",   urlDefault);
            m_LoggingEnabled    = false;
            m_LicenseFile       = ParseStringConfig("LicenseFile", Path.Combine("C:\\MccMotion", "itri-license.lic"));

            m_RestEndpoint      = new RestEndpoint(m_LoggingEnabled);
        }

        private bool ParseBoolConfig(string fieldName, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings[fieldName].ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        private string ParseStringConfig(string fieldName, string defaultValue)
        {
            try
            {
                return ConfigurationManager.AppSettings[fieldName].ToString();
            }
            catch
            {
                return defaultValue;
            }
        }

        private bool IsValidSerialNumber(string serialNumber)
        {
            char[] serialNumberComponents = serialNumber.ToCharArray();

            int[] dashIndex = { 3, 7, 12, 17, 22, 27, 31, 35, 40, 45, 50 };
            int[] charIndex = { 0, 1, 2, 28, 29, 30 };

            if (serialNumberComponents.Length != 55)
            {
                Logging.Write("Invalid Serial Number Length: [ " + serialNumberComponents.Length.ToString() + " ]", m_LoggingEnabled);

                return false;
            }

            foreach (int i in dashIndex)
            {
                if (serialNumberComponents[i] != '-')
                {
                    Logging.Write("Invalid Serial Number: [ " + serialNumberComponents[i] + " | " + i.ToString() +" ]", m_LoggingEnabled);

                    return false;
                }
            }

            foreach (int i in charIndex)
            {
                if (!Char.IsLetter(serialNumberComponents[i]))
                {
                    Logging.Write("Invalid Serial Number: [ " + serialNumberComponents[i] + " | " + i.ToString() + " ]", m_LoggingEnabled);

                    return false;
                }
            }

            for (int i = 0; i < serialNumberComponents.Length; i++)
            {
                if (dashIndex.Contains(i) || charIndex.Contains(i))
                {
                    continue;
                }

                if (!Char.IsDigit(serialNumberComponents[i]))
                {
                    Logging.Write("Invalid Serial Number: [ " + serialNumberComponents[i] + " | " + i.ToString() + " ]", m_LoggingEnabled);

                    return false;
                }
            }

            return true;
        }

        private void text_SerialNumber_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(text_SerialNumber.Text.ToString()))
            {
                string serialNumber = text_SerialNumber.Text.ToString();

                if (serialNumber.Length == 55 && IsValidSerialNumber(serialNumber))
                {
                    text_IdentifierCode.Text = string.Empty;
                    text_Status.Text         = string.Empty;
                    button_Request.Enabled   = true;
                }
                else
                {
                    button_Request.Enabled = false;
                }
            }
        }

        private string getSha256Hash(string text)
        {
            SHA256Managed hashstring = new SHA256Managed();

            byte[] bytes        = Encoding.UTF8.GetBytes(text);
            byte[] hash         = hashstring.ComputeHash(bytes);
            string hashString   = string.Empty;

            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }

        private void button_Request_Click(object sender, EventArgs e)
        {
            text_SerialNumber.ReadOnly  = true;
            text_IdentifierCode.Text    = string.Empty;
            text_Status.Text            = "Running";

            button_Request.Enabled      = false;

            Logging.Reset();

            CMD cmd = new CMD();
            cmd.Run("wmic.exe", "diskdrive get serialnumber", true);

            string       cmdStandardOutput  = cmd.StandardOutput;
            string[]     cmdOutputSplit     = cmdStandardOutput.Split(' ', '\n', '\r');

            List<string> serialNumbers      = new List<string>();

            foreach (string serialNumber in cmdOutputSplit)
            {
                if (string.IsNullOrEmpty(serialNumber) || serialNumber.ToLower() == "serialnumber")
                {
                    continue;
                }

                Logging.Write("HD Serial Number [" + serialNumber + "]", m_LoggingEnabled);

                serialNumbers.Add(serialNumber);
            }

            string hdSerialNumber = string.Empty;

            if (serialNumbers.Count > 0)
            {
                hdSerialNumber = serialNumbers[0];

                Logging.Write("HD Serial Number [" + hdSerialNumber + "] Selected", m_LoggingEnabled);
            }

            Logging.Write("License Key       [" + text_SerialNumber.Text + "]",   m_LoggingEnabled);
            Logging.Write("Machine Signature [" + text_IdentifierCode.Text + "]", m_LoggingEnabled);

            text_IdentifierCode.Text = getSha256Hash(hdSerialNumber);

            m_RestEndpoint.urlAddress    = m_UrlAddress;
            m_RestEndpoint.urlParameters = "{\"licence_key\":\""       + text_SerialNumber.Text + "\"," +
                                            "\"machine_signature\":\"" + text_IdentifierCode.Text + "\"}";
            m_RestEndpoint.LicenseFile   = m_LicenseFile;

            Task task                    = m_RestEndpoint.Query(this);

            text_SerialNumber.ReadOnly   = false;
        }

        internal void ChangeStatus(string message)
        {
            text_Status.Text = message;
        }

    }
}
