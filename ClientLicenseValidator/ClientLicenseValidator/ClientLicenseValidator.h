#pragma once

#include "..\AesLib\aes.h"
#include "..\HdLib\HDSerialNumber.h"
#include "..\ShaLib\sha256.h"

#define BUFFER 2000

#define TRUE  1
#define FALSE 0

//#define CONSOLEDEBUG 1

#define WINDOWS_TICKS_PER_SEC		10000000
#define EPOCH_DIFFERENCE		11644473600LL


#ifdef __cplusplus
extern "C" {
#endif 

	__declspec(dllexport) int IsVerified();

#ifdef __cplusplus
}
#endif