#include "ClientLicenseValidator.h"

double GetLocalEpochTime()
{
	FILETIME	  fileTime = { 0 };
	LARGE_INTEGER largeInt = { 0 };

	GetSystemTimeAsFileTime(&fileTime);
	largeInt.LowPart	= fileTime.dwLowDateTime;
	largeInt.HighPart	= fileTime.dwHighDateTime;
	
	return (double)(largeInt.QuadPart / WINDOWS_TICKS_PER_SEC - EPOCH_DIFFERENCE);
}

bool ReadLicenseFile(char *license, size_t & licenseLength, char *hash, size_t & hashLength, bool enableDebug)
{
	FILE *file;
	char  buffer[BUFFER] = {'\0'};

	file = fopen("C:\\MccMotion\\itri-license.lic", "r");

	if (file == NULL)
	{
		return false;
	}

	size_t charRead  = fread(buffer, 1, BUFFER, file);
	buffer[charRead] = '\0';

	size_t bufferIndex = 0;

	// Find delimiter
	for (licenseLength = 0; bufferIndex < charRead; bufferIndex++, licenseLength++)
	{
		if (buffer[bufferIndex] == '\n')
		{
			bufferIndex++;
			break;
		}

		license[licenseLength] = buffer[bufferIndex];
	}

	for (hashLength = 0; bufferIndex < charRead; bufferIndex++, hashLength++)
	{
		if (buffer[bufferIndex] == '\n' ||
			buffer[bufferIndex] == '\0')
		{
			bufferIndex++;
			break;
		}

		hash[hashLength] = buffer[bufferIndex];
	}

	fclose(file);

	return true;
}

void ParseDelimiter(char * text, size_t textLength, char * parsedText, size_t &parsedLength)
{
	for (size_t i = 0; i < textLength; i++)
	{
		if (text[i] != ':')
		{
			parsedText[parsedLength] = text[i];
			parsedLength++;
		}
	}

	parsedLength++;
}

void ConvertHexStringToUint8t(char * strHexText, size_t strHexLength, uint8_t * uHexText, size_t & uHexLength)
{
	char   parsedText[BUFFER] = {'\0'};
	size_t parsedLength		  = 0;

	ParseDelimiter(strHexText, strHexLength, parsedText, parsedLength);

	for (size_t i = 0; i < parsedLength; i += 2, uHexLength++)
	{
		unsigned int uInt		  = 0;
		char	 charArrayTemp[3] = {'\0'};
		charArrayTemp[0]		  = parsedText[i + 0];
		charArrayTemp[1]		  = parsedText[i + 1];
		
		sscanf(charArrayTemp, "%02x", &uInt);

		uHexText[uHexLength] = (uint8_t)uInt;
	}
}

void ConvertUint8tToString(uint8_t *data, size_t dataLength, char * strData, size_t & strDataLength)
{
	for (strDataLength = 0; strDataLength < dataLength; strDataLength++)
	{
		strData[strDataLength] = (char)data[strDataLength];
	}

	strDataLength++;
}

void ConvertByteToHexString(BYTE * data, char * hexText, size_t & hexTextLength)
{
	for (int i = 0; i < SHA256_BLOCK_SIZE; i++)
	{
		sprintf(&hexText[i * 2], "%02x", data[i]);
		hexTextLength = ((i + 1) * 2) + 1;
	}
}

void ConverUint8tToHexString(uint8_t * data, size_t dataSize, char * uData, size_t & uDataLength)
{
	for (size_t i = 0; i < dataSize; i++)
	{
		sprintf(&uData[i * 2], "%02x", data[i]);
		uDataLength = ((i + 1) * 2) + 1;
	}
}

void Decrypt_Function(uint8_t * key, size_t keySize, uint8_t * data, size_t dataSize, uint8_t * dataBlocks, size_t & dataBlockSize)
{
	uint8_t dataBlock[16 + 1]	= { '\0' };
	size_t	blockIndex			= 0;

	for (size_t dataIndex = 0; dataIndex < dataSize; dataIndex++)
	{
		dataBlock[blockIndex] = data[dataIndex];

		// Block full or EOM
		if ((blockIndex == 15) || (dataIndex == dataSize - 2))
		{
			// EOM and Padding required
			if (blockIndex != 15)
			{
				for (size_t tempIndex = blockIndex; tempIndex < 15; tempIndex++)
				{
					dataBlock[tempIndex] = ((uint8_t) '\0');
				}
			}
		}

		blockIndex++;

		if (blockIndex == 16)
		{
			aes_decrypt_ecb(key, dataBlock);

			for (size_t tempIndex = 0; tempIndex < 16; tempIndex++, dataBlockSize++)
			{
				dataBlocks[dataBlockSize]	= dataBlock[tempIndex];
			}

			blockIndex = 0;
		}
	}

	dataBlockSize++;
}

__declspec(dllexport) int IsVerified()
{
	char hdSerialNumber[BUFFER] = {'\0'};

#ifdef CONSOLEDEBUG
	bool enableDebug = true;
#else
	bool enableDebug = false;
#endif

	//////////////////////////////////////////////////////////////
	// Read license file

	char   encryptedLicense[BUFFER]	= { '\0' };
	size_t encryptedLicenseLength	= 0;

	char   serverSecretHash[BUFFER]	= { '\0' };
	size_t serverSecretHashLength	= 0;

	// Read License File
	if (!ReadLicenseFile(encryptedLicense, encryptedLicenseLength, serverSecretHash, serverSecretHashLength, enableDebug))
	{
		DebugPrint("Failed to parse License File", enableDebug);
		return FALSE;
	}

	for (size_t i = 0; i < 10; i++)
	{
		// Get hard drive serial number from machine
		if (GetHDSerialNumber(i, hdSerialNumber, enableDebug))
		{
			DebugPrint("HD Serial Number: ", hdSerialNumber, enableDebug);

			//////////////////////////////////////////////////////////////
			// Calculate sha256 hash of hard drive 
			BYTE	hashBlock1BYTE[SHA256_BLOCK_SIZE];
			char	hashBlock1[(SHA256_BLOCK_SIZE * 2) + 1] = {'\0'};
			size_t  hashBlock1Length						= 0;

			sha256_hash((const BYTE*)hdSerialNumber, hashBlock1BYTE);
			ConvertByteToHexString(hashBlock1BYTE, hashBlock1, hashBlock1Length);

			DebugPrint("HD Serial Number HASH1: ", hashBlock1, enableDebug);

			//////////////////////////////////////////////////////////////
			// Calculate sha256 hash of (sha256 hash of hard drive)
			BYTE	hashBlock2BYTE[SHA256_BLOCK_SIZE];
			char	hashBlock2[(SHA256_BLOCK_SIZE * 2) + 1] = { '\0' };
			size_t  hashBlock2Length						= 0;

			sha256_hash((const BYTE*)hashBlock1, hashBlock2BYTE);
			ConvertByteToHexString(hashBlock2BYTE, hashBlock2, hashBlock2Length);

			DebugPrint("HD Serial Number HASH2: ", hashBlock2, enableDebug);

			//////////////////////////////////////////////////////////////
			// Decrypt license file and remove padding

			uint8_t encryptionKey[BUFFER]			= {'\0'};
			size_t  encryptionKeyLength				= 0;

			ConvertHexStringToUint8t(hashBlock2,			hashBlock2Length, 
									 encryptionKey,			encryptionKeyLength);

			uint8_t encryptedLicenseUINT[BUFFER]	= { '\0' };
			size_t  encryptedLicenseUINTLength		= 0;

			ConvertHexStringToUint8t(encryptedLicense,		encryptedLicenseLength,
									 encryptedLicenseUINT,	encryptedLicenseUINTLength);

			char   encryptedLicenseHEX[BUFFER]		= {'\0'};;
			size_t encryptedLicenseHEXLength		= 0;

			ConverUint8tToHexString(encryptedLicenseUINT,	encryptedLicenseUINTLength, 
									encryptedLicenseHEX,	encryptedLicenseHEXLength);

			DebugPrint("Encrypted License HASH: ", encryptedLicenseHEX, enableDebug);

			uint8_t decryptedLicenseUINT[BUFFER]	= {'\0'};
			size_t  decryptedLicenseUINTLength		= 0;

			Decrypt_Function(encryptionKey,					encryptionKeyLength,
							 encryptedLicenseUINT,			encryptedLicenseUINTLength,
							 decryptedLicenseUINT,			decryptedLicenseUINTLength);

			char   decryptedLicensePadded[BUFFER]	= {'\0'};;
			size_t decryptedLicensePaddedLength		= decryptedLicenseUINTLength;

			ConvertUint8tToString(decryptedLicenseUINT,		decryptedLicenseUINTLength, 
								  decryptedLicensePadded,	decryptedLicensePaddedLength);

			char	decryptedLicense[BUFFER]		= {'\0'};
			size_t	decryptedLicenseLength			= 0;

			for (size_t decryptedIndex = 0; decryptedIndex < decryptedLicensePaddedLength; decryptedIndex++)
			{
				decryptedLicense[decryptedIndex] = decryptedLicensePadded[decryptedIndex];
				decryptedLicenseLength++;

				if (decryptedLicense[decryptedIndex] == '}')
				{
					decryptedLicenseLength++;
					break;
				}
			}

			DebugPrint("Decrypted License: ", decryptedLicense, enableDebug);

			//////////////////////////////////////////////////////////////

			char	decryptedLocalSecret[BUFFER]	= {'\0'};
			size_t	decryptedLocalSecretLength		= 0;

			for (decryptedLocalSecretLength = 0; decryptedLocalSecretLength < decryptedLicenseLength; decryptedLocalSecretLength++)
			{
				decryptedLocalSecret[decryptedLocalSecretLength] = decryptedLicense[decryptedLocalSecretLength];
			}

			char sharedSecret[BUFFER] = "5>!WB,V8f&T{PL^?";

			for (int i = 0; i < 16; i++)
			{
				decryptedLocalSecret[i + decryptedLocalSecretLength - 1] = sharedSecret[i];
			}

			decryptedLocalSecretLength += 16;

			//////////////////////////////////////////////////////////////
			// Calculate hash of decrypted license and shared secret

			BYTE	decryptedLocalSecretBYTE[SHA256_BLOCK_SIZE];
			char	decryptedLocalSecretHASH[(SHA256_BLOCK_SIZE * 2) + 1]	= { '\0' };
			size_t  decryptedLocalSecretHASHLength							= 0;

			sha256_hash((const BYTE*)decryptedLocalSecret, decryptedLocalSecretBYTE);
			ConvertByteToHexString(decryptedLocalSecretBYTE, decryptedLocalSecretHASH, decryptedLocalSecretHASHLength);
		
			char	encryptedSecretHASHParsed[BUFFER]	= {'\0'};
			size_t	encryptedSecretHASHParsedLength		= 0;

			ParseDelimiter(serverSecretHash,		  serverSecretHashLength, 
						   encryptedSecretHASHParsed, encryptedSecretHASHParsedLength);

			// Return Value 0 They compare equal
			if (strcmp(decryptedLocalSecretHASH, encryptedSecretHASHParsed) != 0)
			{
				DebugPrint("Failed to match server secret hash and local secret hash.", enableDebug);
				DebugPrint("	Server secret hash: ", encryptedSecretHASHParsed, enableDebug);
				DebugPrint("	Local secret hash:  ", decryptedLocalSecretHASH, enableDebug);
				continue;
			}

			//////////////////////////////////////////////////////////////
			// Parse license file for expiration information

			size_t startIndex = 0;
			size_t stopIndex  = 0;

			for (startIndex = 0; startIndex < decryptedLicenseLength - 9; startIndex++)
			{
				if (decryptedLicense[startIndex + 0] == '\'' &&
					decryptedLicense[startIndex + 1] == 'e'  &&
					decryptedLicense[startIndex + 2] == 'x'  &&
					decryptedLicense[startIndex + 3] == 'p'  &&
					decryptedLicense[startIndex + 4] == 'i'  &&
					decryptedLicense[startIndex + 5] == 'r'  &&
					decryptedLicense[startIndex + 6] == 'e'  &&
					decryptedLicense[startIndex + 7] == '\'' &&
					decryptedLicense[startIndex + 8] == ':')
				{
					startIndex = startIndex + 9;
					break;
				}
			}

			for (stopIndex = 1; stopIndex < decryptedLicenseLength; stopIndex++)
			{
				if (decryptedLicense[stopIndex] == '}')
				{
					stopIndex = stopIndex - 1;
					break;
				}
			}

			char timeBuffer[BUFFER] = {'\0'};

			for (size_t i = 0, j = startIndex; j <= stopIndex; i++, j++)
			{
				timeBuffer[i] = decryptedLicense[j];
			}

			double licenseTime	= atof(timeBuffer);
			double localTime	= GetLocalEpochTime();

			// No expiration for licenses with time == 0;
			if (licenseTime == 0)
			{
				return TRUE;
			}

			if (localTime >= licenseTime)
			{
				DebugPrint("License expired.  Local time greater than licence time", enableDebug);
				continue;
			}

			//////////////////////////////////////////////////////////////

			return TRUE;
		}
	}

	DebugPrint("Unable to find a valid HD serial number for decryption", enableDebug);
	return FALSE;
}