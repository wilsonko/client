#pragma once
#pragma warning(disable:4996)

#include <atlstr.h>

bool GetHDSerialNumber(int hdIndex, char * serialNumber, bool enable);

void DebugPrint(char * header, bool enabled);

void DebugPrint(char * header, char * text, bool enabled);
