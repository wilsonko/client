#include "HDSerialNumber.h"

void DebugPrint(char * header, bool enabled)
{
	if (!enabled)
	{
		return;
	}

	printf("%s\n", header);
}

void DebugPrint(char * header, char * text, bool enabled)
{
	if (!enabled)
	{
		return;
	}

	printf("%s %s\n", header, text);
}

DWORD GetPhysicalDriveSerialNumber(UINT nDriveNumber, char * serialNumber, bool enable)
{
    DWORD dwResult = NO_ERROR;

    // Format physical drive path (may be '\\.\PhysicalDrive0', '\\.\PhysicalDrive1' and so on).
	char cUINT[10];
	itoa(nDriveNumber, cUINT, 10);

	char drivePath[100] = "\\\\.\\PhysicalDrive";
	drivePath[17] = cUINT[0];
	drivePath[18] = '\0';

    // call CreateFile to get a handle to physical drive
    HANDLE hDevice = ::CreateFile(drivePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	
	if (INVALID_HANDLE_VALUE == hDevice)
	{
		DebugPrint("Invalid hDevice Handle", enable);
		return ::GetLastError();
	}
    // set the input STORAGE_PROPERTY_QUERY data structure
    STORAGE_PROPERTY_QUERY storagePropertyQuery;
    ZeroMemory(&storagePropertyQuery, sizeof(STORAGE_PROPERTY_QUERY));
    storagePropertyQuery.PropertyId = StorageDeviceProperty;
    storagePropertyQuery.QueryType = PropertyStandardQuery;

    // get the necessary output buffer size
    STORAGE_DESCRIPTOR_HEADER storageDescriptorHeader = { 0 };
    DWORD dwBytesReturned = 0;
    if(!::DeviceIoControl(hDevice, IOCTL_STORAGE_QUERY_PROPERTY,
        &storagePropertyQuery, sizeof(STORAGE_PROPERTY_QUERY),
        &storageDescriptorHeader, sizeof(STORAGE_DESCRIPTOR_HEADER),
        &dwBytesReturned, NULL))
    {
		dwResult = ::GetLastError();
        ::CloseHandle(hDevice);
		itoa(dwResult, cUINT, 10);
		DebugPrint("Invalid DeviceIoController.  Error Code: ", cUINT, enable);
		return dwResult;
    }

    // allocate the necessary memory for the output buffer
    const DWORD dwOutBufferSize = storageDescriptorHeader.Size;
    BYTE* pOutBuffer = new BYTE[dwOutBufferSize];
    ZeroMemory(pOutBuffer, dwOutBufferSize);

    // get the storage device descriptor
    if (!::DeviceIoControl(hDevice, IOCTL_STORAGE_QUERY_PROPERTY,
        &storagePropertyQuery, sizeof(STORAGE_PROPERTY_QUERY),
        pOutBuffer, dwOutBufferSize,
        &dwBytesReturned, NULL))
    {
        dwResult = ::GetLastError();
        delete[]pOutBuffer;
        ::CloseHandle(hDevice);
		itoa(dwResult, cUINT, 10);
		DebugPrint("Invalid Device Descriptor.  Error Code: ", cUINT, enable);
		return dwResult;
    }

    // Now, the output buffer points to a STORAGE_DEVICE_DESCRIPTOR structure
    // followed by additional info like vendor ID, product ID, serial number, and so on.
    STORAGE_DEVICE_DESCRIPTOR* pDeviceDescriptor = (STORAGE_DEVICE_DESCRIPTOR*)pOutBuffer;
    const DWORD dwSerialNumberOffset = pDeviceDescriptor->SerialNumberOffset;
    if (dwSerialNumberOffset != 0)
    {
        // finally, get the serial number
		char * strSerialNumber = reinterpret_cast<char *>(pOutBuffer + dwSerialNumberOffset);

		size_t startIndex = 0;

		for (size_t i = 0; i < 100; i++)
		{
			if (strSerialNumber[i] != ' ')
			{
				startIndex = i;
				break;
			}
		}

		for (size_t i = 0; i < 100; i++)
		{
			serialNumber[i] = strSerialNumber[startIndex + i];

			if (serialNumber[i] == '\0')
			{
				break;
			}
		}
	}

	char tmp[50];
	strcpy_s(&tmp[0], sizeof(tmp) / sizeof(tmp[0]), (char*)(pOutBuffer+dwSerialNumberOffset));

   int index = 0;
   int position = 0;
	//if(lastIndex < firstIndex)
	//	return (String^)NULL;
      //  each integer has two characters stored in it backwards
   char* string = new char[20];
   for (index = 0; index <= 9; index++)
   {
         //  get high byte for 1st character
      string[position] = (char) (tmp [index] / 256);
      position++;

         //  get low byte for 2nd character
      string [position] = (char) (tmp [index] % 256);
      position++;
   }

    // perform cleanup and return
    delete[]pOutBuffer;
    ::CloseHandle(hDevice);
    return dwResult;
}

bool GetHDSerialNumber(int hdIndex, char * serialNumber, bool enable)
{
	DebugPrint("Iterating through available HDs", enable);

	DWORD dwResult = GetPhysicalDriveSerialNumber(hdIndex, serialNumber, enable);

	char cUINT[10];
	itoa(hdIndex, cUINT, 10);

	if (NO_ERROR == dwResult)
	{
		DebugPrint("HD found: Index: ", cUINT, enable);

		return true;
	}

	DebugPrint("Invalid HD found: Index: ", cUINT, enable);

	return false;
}
