#include "stdafx.h"

int main()
{
	char *      fileName		= "ClientLicenseValidator.dll";
	HMODULE		hGetProcIDDLL	= LoadLibrary(fileName);

	DWORD		Result			= 0;
	if (NULL == hGetProcIDDLL)
	{
		Result = GetLastError();

		return EXIT_FAILURE;
	}

	if (!hGetProcIDDLL)
	{
		Result = GetLastError();

		return EXIT_FAILURE;
	}

	FARPROC  dllFunction = GetProcAddress(hGetProcIDDLL, "IsVerified");
	if (!dllFunction)
	{
		Result = GetLastError();
		return EXIT_FAILURE;
	}
	
	Result = dllFunction();

	if (Result == TRUE)
	{
		printf("Pass\n");
	}
	else
	{
		printf("Fail\n");
	}

	return Result;
}

